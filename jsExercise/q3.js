function areaOfScaleneTriangle (firstSide, secondSide, thirdSide){
  let semiPerimeter = (firstSide + secondSide + thirdSide)/2;
  let area = Math.floor(Math.sqrt(semiPerimeter*(semiPerimeter - firstSide) * (semiPerimeter - secondSide) * (semiPerimeter - thirdSide)))
  return area;
}

console.log(areaOfScaleneTriangle(5,6,7))