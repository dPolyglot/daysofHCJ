function addWithSubCharge(firstAmount, secondAmount){
  let sum;
  let chargeLess = 1;
  let chargeMore = 2;

  if(firstAmount > 10){
    firstAmount += chargeMore;
  }else if(secondAmount > 10){
    secondAmount += chargeMore;
  }

  if(firstAmount < 10){
    firstAmount += chargeLess;
  }else if(secondAmount < 10){
    secondAmount += chargeLess;
  }

  sum = firstAmount + secondAmount;

  return sum;
  
}

console.log(addWithSubCharge(30,10))