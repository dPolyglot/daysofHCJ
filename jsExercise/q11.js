function sandwichCalculator(slicesOfBread, slicesOfCheese){
  let possibleNumberOfSandwiches = slicesOfBread/2

  let makeSandwich = 0;

  makeSandwich += slicesOfCheese;

  if(slicesOfCheese > possibleNumberOfSandwiches){
    console.log("Low on bread, high on cheese. Can't make sandwich")
    return;
  }

  return makeSandwich;
}

console.log(sandwichCalculator(6,2))