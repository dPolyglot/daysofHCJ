function rotate(){
  let elements = [];

  let count = 0;

  while(count < 6){
    let input = prompt("Enter a value");
    elements.push(input);
    count++;
  }
  
  let shiftedElement = elements.shift()
  elements.push(shiftedElement)
  
  return elements;
}

console.log(rotate())