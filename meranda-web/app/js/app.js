const leftBtn = document.getElementById("left-btn");
const rightBtn = document.getElementById("right-btn");
const slideOne = document.querySelector(".slider-container--one");
const slideTwo = document.querySelector(".slider-container--two")


leftBtn.addEventListener("click", () => {
  slideTwo.style.display = "none"
  slideOne.style.display = "flex"
})

rightBtn.addEventListener("click", () => {
  slideTwo.style.display = "flex"
  slideOne.style.display = "none"
})


